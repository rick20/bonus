<?php namespace S3\Bonus;

use Carbon\Carbon;
use \AutoDeposit;
use \AutoDepositAudit;
use \Config;
use \User;
use S3\Bonus\Model\BonusStatement;
use S3\Bonus\Model\BonusStatementItem;

class BonusManager {

	private $bonuses = array();
	private $periodes = array();

	public function registerBonus(BonusServiceInterface $bonus)
	{
		$this->bonuses[ $bonus->getSlug() ] = $bonus;
	}

	public function registerPeriode(PeriodeInterface $periode)
	{
		$this->periodes[ $periode->getName() ] = $periode;
	}

	public function getPeriodes()
	{
		return $this->periodes;
	}

	public function getPeriode($name)
	{
		return $this->periodes[ $name ];
	}

	public function hasPeriode($name)
	{
		return array_key_exists($name, $this->periodes); 
	}

	public function allService()
	{
		return $this->bonuses;
	}

	public function getService($slug)
	{
		return $this->bonuses[ $slug ];
	}

	public function hasService($slug)
	{
		return array_key_exists($slug, $this->bonuses);
	}

	public function subscribe($event)
	{
		foreach ($this->bonuses as $bonus_service) {
			foreach ($bonus_service->getEventActions() as $event_name => $action) {
				$event->listen( $event_name, array( $bonus_service, $action ));
			}
		}

		foreach ($this->getPeriodes() as $periode) {
			$event->listen( $periode->getName() . '-bonus.close', array($this, 'generateStatement') );
		}
	}

	public function generateStatement(PeriodeInterface $periode)
	{
		$users = User::with('profile')->where('role_id', '=', 3)->get(); // 3 for member

		foreach ($users as $user) {

			$total = $auto_deposit = 0;
			$statement = false;

			$prev_unpaid = BonusStatement::where('user_id', '=', $user->id)
							 ->where('total_nett', '<', Config::get('bonus::min_paid'))
							 ->where('periode_type', '=', $periode->getName())
							 ->where('status', '=', 'unpaid')
							 ->first();

			foreach ($this->bonuses as $bonus_service) {

				if ($periode->getName() != $bonus_service->periode or ! $bonus_service->statement) {
					continue;
				}

				$params = array(
					'user_id' => $user->id,
					'from' => $periode->getStartDate()->format('Y-m-d H:i:s'),
					'to'   => $periode->getEndDate()->format('Y-m-d H:i:s')
				);

				$bonuses = $bonus_service->search( $params );

				if ($bonuses->count() == 0) {
					continue;
				}

				if (!$statement) {

					$statement = BonusStatement::create(array(
						'user_id' => $user->id,
						'periode_type' => $periode->getName(),
						'periode' => $periode->toString(),
						'status'  => 'unpaid'
					));
				}

				$total_bonus = 0;

				foreach ( $bonuses as $bonus ) {
					$total_bonus += $bonus->total_bonus;
				}

				BonusStatementItem::create(array(
					'statement_id' => $statement->id,
					'bonus'   => $bonus_service->getName(),
					'total'   => $total_bonus
				));

				$total += $total_bonus;

				if ( $bonus_service->auto_deposit ) {
					$auto_deposit += ((Config::get('bonus::auto_deposit')/100) * $total_bonus);
				}
			}

			if ($total > 0) {

				if ($prev_unpaid) {

					$prev_unpaid->status = 'moved';
					$prev_unpaid->save();

					$statement->total_nett_prev  = $prev_unpaid->total_nett;
				}

				$statement->total_gross  = $total;
				$statement->auto_deposit = $auto_deposit;
				$statement->tax 		 = $this->getTax($total - $auto_deposit, $this->getBonusThisYear($user), (bool)$user->profile->npwp);
				$statement->total_nett   = $statement->total_nett_prev + $total - $auto_deposit - $statement->tax;
				$statement->save();

				if ($auto_deposit > 0) {

					$maxDeposit = Config::get('bonus::auto_deposit_max');

					$deposit = AutoDeposit::firstOrCreate(array('user_id' => $user->id));

					$thisMonth = $this->getAutoDepositThisMonth($deposit);

					$withToday = $thisMonth + $auto_deposit;

					$auto_deposit = ($withToday > $maxDeposit) ? $maxDeposit - $thisMonth : $auto_deposit;

					if ($auto_deposit > 0) {

						$deposit->nominal += $auto_deposit;
						$deposit->save();

						AutoDepositAudit::create(array(
							'auto_deposits_id' => $deposit->id,
							'remarks' => 'Auto Deposit dari Statement Bonus ' . $periode->toString(),
							'nominal' => $auto_deposit
						));
					}
				}
			}
		}
	}

	public function getAutoDepositThisMonth($autoDeposit)
	{
		$month = [
			'start' => Carbon::now()->startOfMonth(),
			'end' => Carbon::now()->endOfMonth()
		];

		return AutoDepositAudit::where('auto_deposits_id', $autoDeposit->id)->whereBetween('created_at', $month)->sum('nominal');
	}

	// Pajak things
	// see calculation example: https://trello.com/c/ltXtsvhi/12-pajak
	public function getTax($bonus, $bonusThisYear, $hasNPWP = true)
	{
		$bonusThisYear = ($bonusThisYear + $bonus) / 2;
		$bonus /= 2;

		$classes = array_reverse(Config::get('tax.classes'), true);

		$tax = $pkp = 0;

		foreach ($classes as $percent => $bottom) {

			if ($bonusThisYear < $bottom) {
				continue;
			}

			$pkp = ($bonusThisYear - $bonus >= $bottom) ? $bonus - $pkp : $bonusThisYear - $bottom;

			$tax += $percent / 100 * $pkp;

			if ($bonusThisYear - $bonus >= $bottom) {
				break;
			}

		}

		return $hasNPWP ? $tax : $tax * 1.2;
	}

	public function getBonusThisYear($user)
	{
		$thisYear = array(
			Carbon::now()->startOfYear()->format('Y-m-d H:i:s'),
			Carbon::now()->endOfYear()->format('Y-m-d H:i:s'),
		);

		return BonusStatement::where('user_id', $user->id)->whereBetween('created_at', $thisYear)->sum('total_gross') - 
			   BonusStatement::where('user_id', $user->id)->whereBetween('created_at', $thisYear)->sum('auto_deposit');
	}

}