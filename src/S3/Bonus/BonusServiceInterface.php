<?php namespace S3\Bonus;

use S3\Bonus\PeriodeInterface;

interface BonusServiceInterface {

	public function getName();

	public function getSlug();

	public function getEventActions();

	public function generate($params = null);

	public function search($params = array());
	
}