<?php namespace S3\Bonus;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use \Config;
use \Event;
use S3\Bonus\Command\ClosePeriodeCommand;

class BonusServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	public function boot()
	{
	    $this->package('s3/bonus');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bindShared('bonus_manager', function($app)
		{
			return new BonusManager();
		});

		$this->app->bindShared('command.s3.closeperiode', function($app)
	    {
            return new ClosePeriodeCommand($app);
        });

		$this->app->booting(function($app)
        {
            $loader = AliasLoader::getInstance();
            $loader->alias('BonusManager', 'S3\Bonus\Facade\BonusManagerFacade');

            $bonus_manager = $app['bonus_manager'];
	        $bonus_manager->registerPeriode( new PeriodeWeekly( $app['config']->get('bonus::weekly.closing_day') ) );
			$bonus_manager->registerPeriode( new PeriodeMonthly( $app['config']->get('bonus::monthly.closing_date') ) );
        });

		$this->commands('command.s3.closeperiode');
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('bonus_manager');
	}

}