<?php namespace S3\Bonus\Command;

use \Event;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;
use BonusManager;

class ClosePeriodeCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'bonus:close-periode';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Close Periode Bonus';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		foreach ($this->option() as $periode_name => $enabled) {

			if ( ! $enabled or ! BonusManager::hasPeriode( $periode_name )) continue;
				
			$periode = BonusManager::getPeriode( $periode_name )->lastPeriode();
			
			Event::fire( $periode_name . '-bonus.close',  $periode );
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		$options = array();

		foreach (BonusManager::getPeriodes() as $periode) {

			$options[] = array( $periode->getName(), null, InputOption::VALUE_NONE, 'Generate ' . $periode->getName() . ' bonuses' );

		}

		return $options;
	}

}