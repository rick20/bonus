<?php namespace S3\Bonus\Facade;

use Illuminate\Support\Facades\Facade;

class BonusManagerFacade extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
    	return 'bonus_manager';
    }

}