<?php namespace S3\Bonus\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class BonusStatement extends Eloquent {

	protected $table = 'bonus_statements';

	protected $guarded = array('id', 'created_at', 'updated_at');

	public function statement_items()
    {
        return $this->hasMany('S3\Bonus\Model\BonusStatementItem', 'statement_id');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }
}