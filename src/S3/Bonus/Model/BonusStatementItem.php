<?php namespace S3\Bonus\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class BonusStatementItem extends Eloquent {

	protected $table = 'bonus_statements_items';

	protected $guarded = array('id');

	public $timestamps = false;

	public function statement()
    {
        return $this->belongsTo('BonusStatement');
    }
}