<?php namespace S3\Bonus\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PeriodeBonus extends Eloquent {

	protected $table = 's3new_bonus_periode';

	protected $guarded = array('id', 'created_at', 'updated_at');

	public function bonus_ro()
    {
        return $this->hasMany('BonusRO', 'periode_id');
    }

    public function member()
    {
    	return $this->belongsTo('Member');
    }

    public function close()
	{
		$this->status = 'pending';
        $this->save();
	}

	public function paid()
    {
    	if ($this->status == 'pending') {
	    	$this->status = 'paid';
	    	$this->save();
	    }

    	return $this;
    }
}