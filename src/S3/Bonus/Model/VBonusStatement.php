<?php namespace S3\Bonus\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;

class VBonusStatement extends Eloquent {

	protected $table = 'v_bonus_statements';

	protected $guarded = array('*');

	public function statement_items()
    {
        return $this->hasMany('S3\Bonus\Model\BonusStatementItem', 'statement_id');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }
}