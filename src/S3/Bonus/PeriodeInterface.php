<?php namespace S3\Bonus;

interface PeriodeInterface {

	public function getName();

	public function getStartDate();

	public function getEndDate();

	public function lastPeriode();

	public function currentPeriode();

	public function toPeriode($params);

	public function toString();
}