<?php namespace S3\Bonus;

use Carbon\Carbon;

class PeriodeMonthly implements PeriodeInterface {

	private $start;

	private $close;

	private $closing_date;

	public function __construct($closing_date)
	{
		$this->closing_date = $closing_date;
	}

	public function getName()
	{
		return 'monthly';
	}

	public function getStartDate()
	{
		return $this->start;
	}

	public function getEndDate()
	{
		return $this->close;
	}

	public function lastPeriode()
	{
		$closing_date = Carbon::now()->day( $this->closing_date )->endOfDay();

		if (Carbon::now()->lte( $closing_date )) {
			$closing_date->subMonth();
		}

		$this->close = $closing_date;
		$this->start = $closing_date->copy()->subMonth()->addSecond();

		return $this;
	}

	public function currentPeriode()
	{
		$closing_date = Carbon::now()->day( $this->closing_date )->endOfDay();

		if (Carbon::now()->gt( $closing_date )) {
			$closing_date->addMonth();
		}

		$this->close = $closing_date;
		$this->start = $closing_date->copy()->subMonth()->addSecond();

		return $this;
	}

	public function toPeriode($month_year)
	{
		$closing_date = Carbon::createFromFormat('d-M-Y', $this->closing_date . '-' . $month_year )->endOfDay();

		$this->close = $closing_date;
		$this->start = $closing_date->copy()->subMonth()->addSecond();

		return $this;
	}

	public function toString()
	{
		return $this->close->format('M-Y');
	}
}