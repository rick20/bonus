<?php namespace S3\Bonus;

use Carbon\Carbon;

class PeriodeWeekly implements PeriodeInterface {

	private $start;

	private $close;

	private $closing_day;

	public function __construct($closing_day)
	{
		$this->closing_day = $closing_day;
	}

	public function getName()
	{
		return 'weekly';
	}

	public function getStartDate()
	{
		return $this->start;
	}

	public function getEndDate()
	{
		return $this->close;
	}

	public function lastPeriode()
	{
		$closing_date = Carbon::now()->next( $this->closing_day )->subWeek()->endOfDay();

		$this->close = $closing_date;
		$this->start = $closing_date->copy()->subWeek()->addSecond();

		return $this;
	}

	public function currentPeriode()
	{
		$starting_date = Carbon::now()->previous( $this->closing_day )->addDay();

		$this->start = $starting_date;
		$this->close = $starting_date->copy()->addWeek()->subDay()->endOfDay();

		return $this;
	}

	public function toPeriode($string)
	{
		list( $start, $close ) = explode(' - ', $string);

		$this->start = Carbon::createFromFormat('Y/m/d', $start)->startOfDay();
		$this->close = Carbon::createFromFormat('Y/m/d', $close)->endOfDay();

		return $this;
	}

	public function toString()
	{
		return $this->start->format('Y/m/d') . ' - ' . $this->close->format('Y/m/d');
	}
}