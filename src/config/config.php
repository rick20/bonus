<?php

return array(
	'monthly' => array(
		'closing_date' => 25
	),
	'weekly' => array(
		'closing_day' => \Carbon\Carbon::WEDNESDAY
	),
	'min_paid' => 100000,
	'auto_deposit' => 20, // in percent
	'auto_deposit_max' => 500000,
);